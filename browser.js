/* right click on an entry in the network log, select Copy All as Har
 * type in console: x = [paste]
 * paste the following JS code into the console
 * copy the output, paste into a file
 * then wget -i [that file]
 */
(function(logObj, mime) {
  var results = [];
  logObj.log.entries.forEach(function (entry) {
    if (mime && entry.response.content.mimeType !== mime) return;
    results.push(entry.request.url);
  });
  console.log(results.join('\n'));
})(x, 'image/jpeg');